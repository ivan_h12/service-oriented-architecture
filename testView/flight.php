<?php 
    $seatClass = $_POST["seatClass"];
    $origin = $_POST["origin"];
    $destination = $_POST["destination"];
    $date = $_POST["date"];
    $child = $_POST["child"];
    $adult = $_POST["adult"];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>SOA - REST</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

     <header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0"><a href="index.php" class="text-black h2 mb-0">Travelers</a></h1>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li>
                  <a href="index.php">Home</a>
                </li>
                <li>
                  <a href="booking.html">Booking</a>
                </li>
                
                <li><a href="discount.html">Discount</a></li>
                <li><a href="about.html">About</a></li>
                <li><a href="blog.html">Blog</a></li>
                
                <li><a href="contact.html">Contact</a></li>
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-tripadvisor"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Book A Tour</h1>
              <div><a href="index.php">Home</a> <span class="mx-2 text-white">&bullet;</span> <span class="text-white">Booking</span></div>
              
            </div>
          </div>
        </div>
      </div>  

      <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-7 mb-5">

    <div class="container">
        <div class="row">
            <div class="col-lg-18">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h1>Flight</h1>     
                            </div>
                            <div class="col-lg-6">
                                <div class="text-right">
                                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#lol">
                                      Add Flight
                                    </button>    -->
                                </div>  
                            </div>
                        </div>
                        
                        <table class="table table-hover" id="myTable">
                            <thead>
                                <tr>
                                    <th rowspan="2">Id</th>
                                    <th rowspan="2">Carrier</th>
                                    <th rowspan="2">Departure Date</th>
                                    <th rowspan="2">Arrival Date</th>
                                    <th rowspan="2">Origin</th>
                                    <th rowspan="2">Destination</th>
                                    
                                </tr>
                                <tr>
                                    <th>Price Adult</th>
                                    <th>Price Child</th>
                                    <th>Seat Left</th>
                                    <th>book</th>
                                </tr>
                            </thead>
                            <tbody id="flight-list">
                                
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
    </div>
    </div>
    </div>

    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4">About Travelers</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
            </div>

            
            
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Destination</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">About</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Discounts</a></li>
                </ul>
              </div>
            </div>

            

          </div>

          <div class="col-lg-4 mb-5 mb-lg-0">
           

            <div class="mb-5">
              <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

              <form action="#" method="post">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                  </div>
                </div>
              </form>

            </div>

          </div>
          
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="mb-5">
              <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </div>

            <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
          
        </div>
      </div>
    </footer>

   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

   
        

    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous">
    </script>
    <script>

        function load(){
            $("#flight-list").html("");
            var des = "<?php echo $destination?>";
            var ori = "<?php echo $origin?>";
            var date = "<?php echo $date?>";
            var seatclass = "<?php echo $seatClass?>";
            var counter = parseInt("0");
            $.ajax({
                url: '/flightfiltered/' + ori + "/" + des + "/" + date,
                method: 'GET',
                success: function(data){
                    data.forEach(function(value, index){
                        //var item = '<li>'+ value['nrp'] + ' - ' + value['name'] + '</li>';
                        if(seatclass == 1){
                        var item = '<tr>' + 
                            '<td>' + value['id'] + '</td>' + 
                            '<td>' + value['carrier'] + '</td>' + 
                            '<td>' + value['departure_date'] + '</td>' + 
                            '<td>' + value['arrival_date'] + '</td>' + 
                            '<td>' + value['origin'] + '</td>' + 
                            '<td>' + value['destination'] + '</td>' + 
                            '<td>' + value['price_adult_economy'] + '</td>' + 
                            //'<td>' + value['price_adult_business'] + '</td>' + 
                            //'<td>' + value['price_adult_first'] + '</td>' + 
                            '<td>' + value['price_child_economy'] + '</td>' + 
                            //'<td>' + value['price_child_business'] + '</td>' + 
                            //'<td>' + value['price_child_first'] + '</td>' + 
                            '<td>' + value['seat_left_eco'] + '</td>' + 
                            //'<td>' + value['seat_left_bus'] + '</td>' + 
                            //'<td>' + value['seat_left_fir'] + '</td>' +
                            '<td><button type="submit" class="btn btn-primary py-2 px-4 text-white" id="book" onClick="booked(' + value['id'] + ')">Book</button></td>'+
                            '</tr>';
                        $("#flight-list").append(item);
                    }else if(seatclass == 2){
                        var item = '<tr>' + 
                            '<td>' + value['id'] + '</td>' + 
                            '<td>' + value['carrier'] + '</td>' + 
                            '<td>' + value['departure_date'] + '</td>' + 
                            '<td>' + value['arrival_date'] + '</td>' + 
                            '<td>' + value['origin'] + '</td>' + 
                            '<td>' + value['destination'] + '</td>' + 
                            //'<td>' + value['price_adult_economy'] + '</td>' + 
                            '<td>' + value['price_adult_business'] + '</td>' + 
                            //'<td>' + value['price_adult_first'] + '</td>' + 
                            //'<td>' + value['price_child_economy'] + '</td>' + 
                            '<td>' + value['price_child_business'] + '</td>' + 
                            //'<td>' + value['price_child_first'] + '</td>' + 
                            //'<td>' + value['seat_left_eco'] + '</td>' + 
                            '<td>' + value['seat_left_bus'] + '</td>' + 
                            //'<td>' + value['seat_left_fir'] + '</td>' +
                            '<td><button type="submit" class="btn btn-primary py-2 px-4 text-white" id="book onClick="booked(' + value['id'] + ')>Book</button></td>'+
                            '</tr>';
                        $("#flight-list").append(item);
                    }else if(seatclass == 3){
                        var item = '<tr>' + 
                            '<td>' + value['id'] + '</td>' + 
                            '<td>' + value['carrier'] + '</td>' + 
                            '<td>' + value['departure_date'] + '</td>' + 
                            '<td>' + value['arrival_date'] + '</td>' + 
                            '<td>' + value['origin'] + '</td>' + 
                            '<td>' + value['destination'] + '</td>' + 
                            //'<td>' + value['price_adult_economy'] + '</td>' + 
                            //'<td>' + value['price_adult_business'] + '</td>' + 
                            '<td>' + value['price_adult_first'] + '</td>' + 
                            //'<td>' + value['price_child_economy'] + '</td>' + 
                            //'<td>' + value['price_child_business'] + '</td>' + 
                            '<td>' + value['price_child_first'] + '</td>' + 
                            //'<td>' + value['seat_left_eco'] + '</td>' + 
                            //'<td>' + value['seat_left_bus'] + '</td>' + 
                            '<td>' + value['seat_left_fir'] + '</td>' +
                            '<td><button type="submit" class="btn btn-primary py-2 px-4 text-white book" onClick="booked(' + value['id'] + ')>Book</button></td>'+
                            '</tr>';
                        $("#flight-list").append(item);
                    }
                    });

                }
            });
        }

        function booked(data){
            var id_flight = data;
            var seat_economy = 0;
            var seat_business = 0;
            var seat_first = 0;
            var status_seat = "<?php echo $seatClass?>";
            var num_child = parseInt("<?php echo $child?>");
            var num_adult = parseInt("<?php echo $adult?>");
            var id_user = 1;
            if(status_seat == 1){
                seat_economy = num_adult + num_child;
            }else if(status_seat == 2){
                seat_business = num_adult + num_child;
            }else if(status_seat == 3){
                seat_first = num_adult + num_child;
            }
            // console.log(id_flight);
            // console.log(seat_economy);
            // console.log(seat_business);
            // console.log(seat_first);
            // console.log(status_seat);
            // console.log(num_child);
            // console.log(num_adult);

            var send_data = {
                "id_flight" : id_flight,
                "seat_economy" : seat_economy,
                "seat_business": seat_business,
                "seat_first": seat_first,
                "status_seat": status_seat,
                "num_child": num_child,
                "num_adult": num_adult,
                "id_user": 1
            };
            $.ajax({
                url: '/bookflight',
                headers: {
                        'Content-Type' : 'application/json'
                },
                method: 'POST',
                data: JSON.stringify(send_data),
                success: function(data){
                    if(data['err'] == 0){
                        alert("Booking success");
                    }
                }
            });
        }

        $(document).ready(function(){
            load();
            var adult = "<?php $adult?>"
            var child = "<?php $child?>"
            
            $(".book").click(function(){
                var send_data = {
                    'nrp' : $("#nrp").val(),
                    'name' : $("#name").val()

                };
                $.ajax({
                    url: '/api/user',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    method: 'POST',
                    data: JSON.stringify(send_data),
                    success: function(data){
                        if(data['err'] == 0){
                            load();
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>