<?php
  include "header.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>GO-REV - Travel Advisor</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
  </head>

  <body>
  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <div>
      
    </div>

  
    <div class="row">
    <div class="col-md-2">
      
    </div>
    <div class="col-md-8">
      <div class="thumbnail" id="detail_wisata">
        
          
          <!-- <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div> -->
        
      </div>
      <div id="googleMap" style="width:50%;height:400px;"></div>
      <div class="card" style="margin-top: 10px;">
        <center><h2>Comment List</h2></center>
        <div id="isi_comment">
          
        </div>
        

      </div>

    </div>
    <div class="col-md-2">
      
    </div>
  </div>
    



    

    <!-- <div class="site-section bg-light">
    </div> -->
    <?php include "footer.php" ?>
  </div>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script>

            function load(){
                // var id_category = window.location.search;
                var url_string = window.location.href;
                var url = new URL(url_string);
                var id = url.searchParams.get("id");
                
                $("#detail_wisata").html("");
                $.ajax({
                    url: '/api/wisata/detail/'+id,
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){

                            var item = '<h1>'+value['name']+'</h1>'
                            +'<h3> Rating : '+ value['rating']+' / 5 <span class="fa fa-star checked" style="color:orange;"></span> ('+value['rating_count']+' Ulasan)</h3>'
                            +''
                            +'<img src="images/03-japan.jpg" alt="Nature" style="width:100%; height: 50%">'
                            +'<h3>Deskripsi</h3>'
                            +'<p>'+value['description']+'</p>'
                            +'<h3>Location</h3>'
                            +'<p>'+value['location']+'</p>';
                            $("#detail_wisata").append(item);
                            
                        });
                    }
                });

                $("#isi_comment").html("");
                $.ajax({
                    url: '/api/comment/'+id,
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){

                            var items = '<div class="card" style="margin-left:10px;margin-right:10px;margin-bottom: 10px;">'
                            +'<div class="card-body" >'
                            +'<b>'+value['email']+'</b> : '+value['comment']
                            +'</div>'
                            +'</div>'; 
                            
                            $("#isi_comment").append(items);
                            
                        });
                    }
                });

                
            }

            $(document).ready(function(){
                load();

                
            });
            function myMap() {
              var mapProp= {
                  center:new google.maps.LatLng(-7.274635, 112.782292),
                  zoom:9.2,
              };
              var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            }

            
        </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARMfnT7k5gV975zlux03Cd2bUXrPa8cC0&callback=myMap"></script>  
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>