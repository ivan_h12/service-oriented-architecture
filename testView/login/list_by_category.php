<?php
  

  session_start();
  if (!isset($_SESSION["email"])) {
    session_destroy();
    header("Location: ../index.php");
  }
  else
  {
    include "header.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>GO-REV - Travel Advisor</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>

  <body>
  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <div>
      
    </div>

  
    <!-- kategori -->
    <div class="site-section">
      
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black" id="title"></h2>
            <p class="color-black-opacity-5">Choose Your Next Destination</p>
          </div>
        </div>
        <div class="row" id="category_list">
          
          
        </div>
      </div>
    
    </div>
    



    

    <!-- <div class="site-section bg-light">
    </div> -->
    <?php include "footer.php" ?>
  </div>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script>

            function load(){
                // var id_category = window.location.search;
                var url_string = window.location.href;
                var url = new URL(url_string);
                var id_category = url.searchParams.get("id");
                
                $("#category_list").html("");
                $.ajax({
                    url: '/api/wisata/'+id_category,
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){
                            var item = '<div class="col-md-6 col-lg-4 mb-4 mb-lg-4">'
                                          +'<a href="detail_wisata.php?id='+value['id']+'" class="unit-1 text-center">'
                                            +'<img src="../img/1.jpg" alt="Image" class="img-fluid">'
                                            +'<div class="unit-1-text">'
                                              +'<h3 class="unit-1-heading">'+value['name']+'</h3>'
                                            +'</div>'
                                        +'</a>'
                                      +'</div>';
                            $("#category_list").append(item);
                        });
                    }
                });

                $("#title").html("");
                $.ajax({
                    url: '/api/category/'+id_category,
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){
                            var item = value['name'];
                            $("#title").append(item);
                        });
                    }
                });
            }

            $(document).ready(function(){
                load();

                
            });

            
        </script>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>
<?php
}
?>