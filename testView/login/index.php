<?php
  

  session_start();
  if (!isset($_SESSION["email"])) {
    session_destroy();
    header("Location: ../index.php");
  }
  else
  {
    include "header.php";

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>GO-REV - Travel Advisor</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>

  <body>
  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <div class="slide-one-item home-slider owl-carousel">
      
      <div class="site-blocks-cover overlay" style="background-image: url(images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              

              <h1 class="text-white font-weight-light">Never Stop Exploring</h1>
              <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga est inventore ducimus repudiandae.</p>
              <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Book Now!</a></p>

            </div>
          </div>
        </div>
      </div>  

      <div class="site-blocks-cover overlay" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Love The Places</h1>
              <p class="mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga est inventore ducimus repudiandae.</p>
              <p><a href="#" class="btn btn-primary py-3 px-5 text-white">Book Now!</a></p>
            </div>
          </div>
        </div>
      </div>  

    </div>

    <!-- Gambar atas -->
    <div class="site-section">
      
      <div class="container overlap-section">
        <center><h1 style="color: white;">Most Visited</h1></center>
        <div class="row" id="category_list">

          
        </div>
      </div>
    
    </div>
  
    <!-- panah dibawah testimonial -->
    <div class="site-section">
       <div class="container overlap-section">
        <center><h1 style="color: black;">Recommended For You</h1></center>
        <div class="row" id="recommend_list">

          
        </div>
      </div>
    
    </div>



    <!-- kategori -->
    <div class="site-section">
      
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black">Our Destinations</h2>
            <p class="color-black-opacity-5">Choose Your Next Destination</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=1" class="unit-1 text-center">
              <img src="../img/1.jpg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Shopping Mall</h3>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=2" class="unit-1 text-center">
              <img src="../img/2.jpeg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Nature Reserve</h3>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=3" class="unit-1 text-center">
              <img src="../img/3.jpg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Amusement Park</h3>
              </div>
            </a>
          </div>

          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=4" class="unit-1 text-center">
              <img src="../img/4.jpg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Extreme Sport</h3>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=5" class="unit-1 text-center">
              <img src="images/05-london.jpg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Historical</h3>
              </div>
            </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="list_by_category.php?id=6" class="unit-1 text-center">
              <img src="images/06-australia.jpg" alt="Image" class="img-fluid">
              <div class="unit-1-text">
                
                <h3 class="unit-1-heading">Culinary</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    
    </div>

    <!-- <div class="site-section bg-light">
    </div> -->
    <?php include "footer.php" ?>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script>

            function load(){
                
                $("#category_list").html("");
                $.ajax({
                    url: '/api/wisata_most/',
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){
                            var item = '<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">'
                                          +'<a href="detail_wisata.php?id='+value['id']+'" class="unit-1 text-center">'
                                            +'<img src="../img/1.jpg" alt="Image" class="img-fluid">'
                                            +'<div class="unit-1-text">'
                                              +'<h3 class="unit-1-heading">'+value['name']+'</h3>'
                                            +'</div>'
                                        +'</a>'
                                      +'</div>';
                            $("#category_list").append(item);
                        });
                    }
                });
                $("#recommend_list").html("");
                $.ajax({
                    url: '/api/recommended/',
                    method: 'GET',
                    success: function(data){
                        data.forEach(function(value, index){
                            var item = '<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">'
                                          +'<a href="detail_wisata.php?id='+value['id']+'" class="unit-1 text-center">'
                                            +'<img src="../img/1.jpg" alt="Image" class="img-fluid">'
                                            +'<div class="unit-1-text">'
                                              +'<h3 class="unit-1-heading">'+value['name']+'</h3>'
                                            +'</div>'
                                        +'</a>'
                                      +'</div>';
                            $("#recommend_list").append(item);
                        });
                    }
                });

            }

            $(document).ready(function(){
                load();

                
            });

            
        </script>
    
  </body>
</html>
<?php
}
?>