<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'database.php';

$app = new \Slim\App;

// ================================================================================
// --------------------------------------USER--------------------------------------
// ================================================================================

#get_all
$app->get('/user', function(Request $request, Response $response, array $args){
    $data = [];
    $response = $response->withHeader('Content-type', 'application/json');
    global $con;
    $sql = "SELECT * FROM users";
    $res = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[]=$row;
    }
    $response->getBody()->write(json_encode($data));
    return $response;
});

#get_user
$app->get('/user/{id}', function (Request $request, Response $response, array $args){
    global $con;
    $id = $args['id'];
    $sql = "SELECT * FROM users WHERE id=".$id;
    $res = mysqli_query($con, $sql);
    $data = mysqli_fetch_assoc($res);

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#get_user_byName
$app->get('/user/Name/{name}', function (Request $request, Response $response, array $args){
    $name= $args['name'];
    global $con;
    $data =[];
    $sql = "SELECT * FROM users WHERE name LIKE '%".$name."%'";
    $res = mysqli_query($con,$sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[]=$row;
    }
    
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#get_user_byEmail
$app->get('/user/Email/{email}', function (Request $request, Response $response, array $args){
    $email= $args['email'];
    global $con; 
    $data = [];
    $sql =  "SELECT * FROM users WHERE email LIKE '%".$email."%'";
    $res = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[]= $row;
    }

    $response = $response-> withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#add_user
$app->post('/user', function (Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();

    $status = array(
        'err'=>0,
        'msg'=>''
    );

    global $con;

    $sql = 'INSERT INTO users VALUES(default, "'.$obj["name"].'", "'.$obj["birthdate"].'", '.$obj["gender"].', "'.$obj["email"].'", "'.$obj["address"].'", "'.$obj["username"].'", "'.$obj["password"].'")';
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#update_user
$app->put('/user/update/{id}', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();
    $id = $args['id'];
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    global $con;

    $sql = 'UPDATE users SET name="'.$obj["name"].'", birthdate="'.$obj["birthdate"].'", gender='.$obj["gender"].', email="'.$obj["email"].'", address="'.$obj["address"].'", username="'.$obj["username"].'" WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#update_password_user
$app->put('/user/pass/{id}', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();
    $id = $args['id'];
    $status = array(
        'err'=>0, 
        'msg'=>''
    );

    global $con; 
    $sql = 'UPDATE users SET password="'.$obj["password"].'" WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));

    return $response;
});

#delete_user
$app->delete('/user/del/{id}', function(Request $request, Response $response, array $args){
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    $id = $args['id'];

    global $con; 
    $sql = 'DELETE FROM users WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=2;
        $status['msg']="Error deleting entry";
    }
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

// ================================================================================
// --------------------------------------AUTH--------------------------------------
// ================================================================================

#get_auth
$app->post('/user/:email/:user_password', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();

    $email = $args['email'];
    $pass = $args['user_password'];

    $status = array(
        'err'=>0,
        'msg'=>''
    );

    global $con;
    $sql = 'SELECT * FROM users WHERE email="'.$email.'" AND password="'.$pass;

    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=3;
        $statsu['msg']='Invalid Email/Password';
    }
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
});

// ================================================================================
// -------------------------------------FLIGHT-------------------------------------
// ================================================================================

$app->get('/flight', function(Request $request, Response $response, array $args) {
    global $con;
    $data = [];
    $sql = "SELECT * FROM flight a,seat b,price c WHERE a.id_seat = b.id AND a.id_price = c.id";
    $res = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[] = $row;
    }


    $response = $response->withHeader('Content-Type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#get_flight_by_id
$app->get('/flight/{id}', function (Request $request, Response $response, array $args){
    global $con;
    $id = $args['id'];
    $sql = "SELECT * FROM flight a,seat b , price c WHERE a.id_seat = b.id AND a.id_price = c.id AND a.id=".$id;
    $res = mysqli_query($con, $sql);
    $data = mysqli_fetch_assoc($res);
    
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#add_price
$app->post('/flightprice', function (Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();

    $status = array(
        'err'=>0,
        'msg'=>''
    );
    global $con;
    
    $sql = 'INSERT INTO price VALUES(default, '.$obj["price_adult_economy"].', '.$obj["price_adult_business"].', '.$obj["price_adult_first"].', '.$obj["price_child_economy"].', '.$obj["price_child_business"].', '.$obj["price_child_first"].')';
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#add_seat
$app->post('/flightseat', function (Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();

    $status = array(
        'err'=>0,
        'msg'=>''
    );
    global $con;
    
    $sql = 'INSERT INTO seat VALUES(default, '.$obj["seat_cap_eco"].', '.$obj["seat_cap_bus"].', '.$obj["seat_cap_fir"].', '.$obj["seat_left_eco"].', '.$obj["seat_left_bus"].', '.$obj["seat_left_fir"].')';
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#add_flight
$app->post('/flight', function (Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();

    $status = array(
        'err'=>0,
        'msg'=>''
    );
    global $con;
    
    $sql = 'INSERT INTO flight VALUES(default, "'.$obj["carrier"].'", "'.$obj["departure_date"].'", "'.$obj["arrival_date"].'", "'.$obj["orgin"].'", "'.$obj["destination"].'", '.$obj["id_seat"].', '.$obj["id_price"].')';
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#delete_price
$app->delete('/price/del/{id}', function(Request $request, Response $response, array $args){
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    $id = $args['id'];

    global $con; 
    $sql = 'DELETE FROM price WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=2;
        $status['msg']="Error deleting entry";
    }
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#delete_seat
$app->delete('/seat/del/{id}', function(Request $request, Response $response, array $args){
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    $id = $args['id'];

    global $con; 
    $sql = 'DELETE FROM seat WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=2;
        $status['msg']="Error deleting entry";
    }
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#delete_flight
$app->delete('/flight/del/{id}', function(Request $request, Response $response, array $args){
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    $id = $args['id'];

    global $con; 
    $sql = 'DELETE FROM flight WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=2;
        $status['msg']="Error deleting entry";
    }
    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#update_price
$app->put('/price/update/{id}', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();
    $id = $args['id'];
    $status = array(
        'err'=>0,
        'msg'=>''
    );
    global $con;

    $sql = 'UPDATE price SET price_adult_economy='.$obj["price_child_economy"].', price_adult_business='.$obj["price_adult_business"].', price_adult_first='.$obj["price_adult_fir"].', price_child_economy='.$obj["price_child_economy"].', price_child_business='.$obj["price_child_business"].', price_child_first='.$obj["price_child_first"].' WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#update_seat
$app->put('/seat/update/{id}', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();
    $id = $args['id'];
    $status = array(
        'err'=>0,
        'msg'=>''
    );
    global $con;

    $sql = 'UPDATE seat SET seat_cap_eco='.$obj["seat_cap_eco"].', seat_cap_bus='.$obj["seat_cap_bus"].', seat_cap_fir='.$obj["seat_cap_fir"].', seat_left_eco='.$obj["seat_left_eco"].', seat_left_bus='.$obj["seat_left_bus"].', seat_left_fir='.$obj["seat_left_fir"].' WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#update_flight
$app->put('/flight/update/{id}', function(Request $request, Response $response, array $args){
    $obj = $request->getParsedBody();
    $id = $args['id'];
    $status = array(
        'err'=>0,
        'msg'=>''
    );

    global $con;

    $sql = 'UPDATE users SET carrier="'.$obj["carrier"].'", departure_date="'.$obj["departure_date"].'", arrival_date="'.$obj["arrival_date"].'", origin="'.$obj["origin"].'", destination="'.$obj["destination"].'", id_price='.$obj["id_price"].', id_seat='.$obj["id_seat"].' WHERE id='.$id;
    $res = mysqli_query($con, $sql);

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});

#filtered_flight
$app->get('/flightfiltered/{origin}/{destination}/{departure_date}', function(Request $request, Response $response, array $args) {
    global $con;
    $data = [];
    $origin = $args['origin'];
    $destination = $args['destination'];
    $departure_date = $args['departure_date'];
    $sql = "SELECT a.*, b.seat_cap_eco, b.seat_cap_bus, b.seat_cap_fir, b.seat_left_eco, b.seat_left_bus, b.seat_left_fir, c.price_adult_economy, c.price_adult_business, c.price_adult_first, c.price_child_economy, c.price_child_economy, c.price_child_first FROM flight a,seat b , price c WHERE a.id_seat = b.id AND a.id_price = c.id AND a.origin='".$origin."'AND a.destination='".$destination."' AND a.departure_date LIKE '%".$departure_date."%'";
    $res = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[] = $row;
    }
    $response = $response->withHeader('Content-Type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#search
$app->post('/flight/search', function(Request $request, Response $response, array $args){
    global $con;
    $data=[];
    $obj = $request->getParsedBody();
    $ordsum=$obj['adult']+$obj['child'];
    $seat = "c.seat_left_".$obj['seat'];
    if($obj['seat']=='eco'){
        $pricea = "b.price_adult_economy";
    $priceb = "b.price_child_economy";        
    }
    else if($obj['seat']='bus'){
        $pricea = "b.price_adult_business";
    $priceb = "b.price_child_business";
    }
    else if($obj['seat']='first'){
        $pricea = "b.price_adult_first";
    $priceb = "b.price_child_first";
    }
    else{ 
        return "error";
    }
    
    $sql = "SELECT a.*, ".$seat.", ".$pricea.", ".$priceb." FROM flight a INNER JOIN seat c ON a.id_seat=c.id INNER JOIN price b ON a.id_price=b.id WHERE a.origin LIKE '".$obj['origin']."' AND a.destination LIKE '".$obj['destination']."' AND a.departure_date LIKE '%".$obj['dep_date']."%' AND ".$seat." >=".$ordsum;

    $res=mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($res)){
        $data[]=$row;
    }

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
});

#booking_flight
$app->post('/bookflight', function (Request $request, Response $response, array $args){
   /*  {
    "id_flight" : 2,
    "seat_economy" :3,
    "seat_business":0,
    "seat_first":0,
    "status_seat":1,
    "num_child":1,
    "num_adult":2,
    "id_user":1
}*/
    global $con;
    $obj = $request->getParsedBody();
    $sql = "SELECT a.*, b.seat_cap_eco, b.seat_cap_bus, b.seat_cap_fir, b.seat_left_eco, b.seat_left_bus, b.seat_left_fir, c.price_adult_economy, c.price_adult_business, c.price_adult_first, c.price_child_economy, c.price_child_economy, c.price_child_first FROM flight a,seat b ,price c WHERE a.id_seat = b.id AND a.id_price = c.id AND a.id=".$obj['id_flight'];
    $res = mysqli_query($con, $sql);
    $data = mysqli_fetch_assoc($res);

     $status = array(
        'err'=>0,
        'msg'=>'',
    );

    $count_economy = $obj['seat_economy'];
    $count_business = $obj['seat_business'];
    $count_first = $obj['seat_first'];
    $final_eco = $data['seat_left_eco'] - $count_economy;
    $final_bus = $data['seat_left_bus'] - $count_business;
    $final_fir = $data['seat_left_fir'] - $count_first;

    $sql = 'UPDATE seat SET seat_left_eco='.$final_eco.', seat_left_bus='.$final_bus.', seat_left_fir='.$final_fir.' WHERE id='.$data['id_seat'];
    $res = mysqli_query($con, $sql);
    
     if($obj['status_seat']==1){
        $total = $obj["num_child"] * $data["price_child_economy"] + $obj["num_adult"] * $data["price_adult_economy"];
    }
    if($obj['status_seat']==2){
        $total = $obj["num_child"] * $data["price_child_business"] + $obj["num_adult"] * $data["price_adult_business"];
    }
    if($obj['status_seat']==3){
        $total = $obj["num_child"] * $data["price_child_first"] + $obj["num_adult"] * $data["price_adult_first"];
    }
     $sql = 'INSERT INTO booking VALUES (default, '.$obj["id_user"].', '.$obj["id_flight"].', '.$obj["status_seat"].', '.$obj["num_child"].', '.$obj["num_adult"].', '.$total.')';
    $res = mysqli_query($con, $sql);   

    if(!$res){
        $status['err']=1;
        $status['msg']="Error inserting to databse";
    }

    $response = $response->withHeader('Content-type', 'application/json');
    $response->getBody()->write(json_encode($status));
    return $response;
});
//=================================================================================
$app->run();